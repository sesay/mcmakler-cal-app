import React from 'react';
import Layout from './Layout/Layout';
import Calendar from './Component/Calendar/Calendar';
import './App.css';

function App() {
  return (
    <div className="App">
      <Layout>
        <Calendar />
      </Layout>
    </div>
  );
}

export default App;
