import React from 'react';
import './TimeSlot.css';

const TimeSlot = props => {
  const { appointmentDate, appointmentTime, appointmentTimeSlot } = props;
  const timeSlotArray = [
    '09:00',
    '10:00',
    '11:00',
    '12:00',
    '13:00',
    '14:00',
    '15:00',
    '16:00',
    '17:00',
    '18:00'
  ];
  return (
    <div className="timeslot">
      <h2>Folgende Uhrzeiten sind für den <span>{appointmentDate}</span> verfügbar</h2>
      <div className="time-table">
        <ul>
          {timeSlotArray.map(i => <li key={i}> <button onClick={appointmentTime.bind(this, i)} className={appointmentTimeSlot === i ? 'active' : ''}>{i}</button> </li>) }
        </ul>
      </div>
    </div>
  )
}

export default TimeSlot;