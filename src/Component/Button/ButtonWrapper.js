import React from 'react';

const ButtonWrapper = props => {
  return (
    <div className="btn-wrapper-container">
      {props.children}
    </div>
  )
}

export default ButtonWrapper;
