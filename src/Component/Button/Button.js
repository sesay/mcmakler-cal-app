import React from 'react';
import PropTypes from 'prop-types';
import './Button.css';

const Button = props => {
  const { showNextStep, showPrevStep, disabled } = props;
  return (
     <div className={showPrevStep ? 'btn-wrapper back-btn' : 'btn-wrapper'}>
        <button
          onClick={showNextStep || showPrevStep}
          disabled={!disabled}
          className={disabled ? 'active' : ''}
        >
          {props.children}
        </button>
      </div>
    )
}

Button.propTypes = {
  showNextStep: PropTypes.func,
  showPrevStep: PropTypes.func,
  disabled: PropTypes.bool,
};


export default Button;