import React from 'react';

const AppointmentDetails = props => {
  const { date, time } = props;
  console.log(props);
  return (
    <div>
      <h2> Thank you for booking your appointment</h2>
      <br />
      <p> You have an appointment schedule on <strong>{date}</strong> at <strong>{ time }</strong></p>
    </div>
  )
}

export default AppointmentDetails;
