import React from 'react';
import Calendar from 'react-calendar';
import TimeSlot from './../../Component/TimeSlot/TimeSlot';
import DateTimeStamp from './../../Component/DateTimeStamp/DateTimeStamp';
import AppointmentDate from './../../Component/DateTimeStamp/AppointmentDate/AppointmentDate';
import AppointmentTime from './../../Component/DateTimeStamp/AppointmentTime/AppointmentTime';
import ButtonWrapper from './../../Component/Button/ButtonWrapper';
import Button from './../../Component/Button/Button';
import { FaChevronRight, FaChevronLeft } from 'react-icons/fa';
import CalendarWrapper from './../../Component/CalendarWrapper/CalendarWrapper';
import './Calendar.css';
import AppointmentDetails from './../../Component/AppointmentDetails/AppointmentDetail';

// Declare the Internalization Constant
var options = { year: 'numeric', month: 'short', day: 'numeric' };
class CalendarApp extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      date: '',
      appointmentTime: '',
      step: 1,
    }
  }

  onChange = date => {
    const dateLocale = new Intl.DateTimeFormat('de-DE', options).format(date);

    this.setState(
      {
        date: dateLocale,
        activeDate: date
      }
    );
    
  }

  prevStep = () => {
    const { step } = this.state;
    if (step > 1) {
      this.setState({ step: step - 1 })
    }
  }

  nextStep = () => {
    const { step } = this.state;
    this.setState(
      {
        step: step + 1
      }
    );
  }

  appointmentTimeSlot = (value) => {
    this.setState({
      appointmentTime: value,
    });
  }

  render() {
    const { step } = this.state;
    switch (step) {
      case 1:
        return (
          <CalendarWrapper
            showPrevStep={this.prevStep}
            showHeader
          >
            <Calendar
                onChange={this.onChange}
                locale="de-DE"
                minDate={new Date()}
                showNeighboringMonth={false}
                prevLabel=""
                nextLabel=""
                value={this.state.activeDate}
            />
            <DateTimeStamp>
              <AppointmentDate
                date={this.state.date}
              />
              <AppointmentTime
                time={this.state.appointmentTime}
              />
              <ButtonWrapper>
                <Button
                  showNextStep={this.nextStep}
                  disabled={this.state.date !== ''}
                >
                  Weiter <FaChevronRight  />
                </Button>
                <Button
                  showPrevStep={this.prevStep}
                  disabled
                >
                  <FaChevronLeft />
                </Button>
              </ButtonWrapper>
            </DateTimeStamp>
          </CalendarWrapper>
        )
      case 2: 
        return (
          <CalendarWrapper
            showPrevStep={this.prevStep}
            showHeader
          >
            <TimeSlot
                appointmentDate={this.state.date}
                appointmentTimeSlot={this.state.appointmentTime}
                appointmentTime={this.appointmentTimeSlot}
              />
              <DateTimeStamp>
                <AppointmentDate
                  date={this.state.date}
                />
                <AppointmentTime
                  time={this.state.appointmentTime}
                />
                <ButtonWrapper>
                  <Button
                    showNextStep={this.nextStep}
                    disabled={this.state.appointmentTime !== ''}
                    >
                    Weiter <FaChevronRight  />
                  </Button>
                  <Button
                    showPrevStep={this.prevStep}
                    disabled
                  >
                  <FaChevronLeft />
                </Button>
                </ButtonWrapper>
              </DateTimeStamp>
          </CalendarWrapper>
        )
      case 3: 
        return (
          <CalendarWrapper showHeader={false}>
            <AppointmentDetails
              date={this.state.date}
              time={this.state.appointmentTime}
            />
          </CalendarWrapper>
        )
      default:
        break;
    }
  }
};

export default CalendarApp;