import React from 'react';

const AppointmentTime = props => {
  const { time } = props;
  return (
    <div className="appointment-time">
      <p>{time}</p>
    </div>
  )
}

export default AppointmentTime;
