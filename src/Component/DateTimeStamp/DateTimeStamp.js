import React from 'react';
import './DateTimeStamp.css';

const DateTimeStamp = props => {
  return (
    <div className="date-stamp">
      <h3>Ihr Termin ist am:</h3>
      { props.children}
    </div>
  )
}

export default DateTimeStamp;
