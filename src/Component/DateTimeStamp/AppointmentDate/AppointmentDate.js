import React from 'react';

const AppointmentDate = props => {
  const { date } = props;
  return (
    <div className="appointment-date">
      <p>{ date }</p>
    </div>
  )
}

export default AppointmentDate;
