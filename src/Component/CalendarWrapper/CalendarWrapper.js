import React from 'react';
import { FaChevronLeft } from 'react-icons/fa';

const CalendarWrapper = props => {
  const { showHeader, showPrevStep } = props;
  return (
      <div id="calendar-wrapper">
        {
          showHeader && (
            <header className="header">
              <div className="button-wrapper">
                <button onClick={showPrevStep}>
                  <FaChevronLeft />
                </button>
              </div>
              <h2> Wählen Sie einen Termin aus.</h2>
            </header>
          )
        }
      <div className="calendar-container">
        { props.children }
      </div>
    </div>
  )
}

export default CalendarWrapper;
