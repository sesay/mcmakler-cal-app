import React from 'react';
import './Layout.css';

const Layout = props => {
  return (
    <div className="container-content">
      {props.children}
    </div>
  )
}

export default Layout;